#ifndef SQLITEHANDLE_H
#define SQLITEHANDLE_H

#include <QObject>

#include <qdebug.h>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>

class SqliteHandle : public QObject
{
    Q_OBJECT
public:
    explicit SqliteHandle(QObject *parent = nullptr);
    ~SqliteHandle();

    //查找某个字段值是否存在
    bool checkExist(QString tableName,QString key,QString value);
    //根据 已知字段key的value值查找目标字段targetKey的value值
    QVariant getValue(QString tableName,QString key,QString value,QString targetKey);
signals:

public slots:

private:
    QSqlDatabase m_sqlDB;

    void initDb();
    void creatUserTable();
};

#endif // SQLITEHANDLE_H
