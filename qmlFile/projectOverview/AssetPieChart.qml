import QtQuick 2.0
import QtQuick.Controls 2.12
import QtCharts 2.13

import EUIpackage 1.0
import EControl 1.0


Rectangle{
    width: 598
    height: 376
    color: "white"
    radius: 4
    property real allPeopelCount:0
    property alias pieSeries: pieSeries

    // 圆心
    property var pieMidPoint: Qt.point(rootChartview.plotArea.width/2 + rootChartview.plotArea.x,rootChartview.plotArea.height/2 + rootChartview.plotArea.y)

    //左上角标题
    Text {
        z:1
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: 24
        width: contentWidth
        height: contentHeight
        text: qsTr("资产配置达成")
        color: EColor.textColor(EColor.Text_Main)
        font.pixelSize: 16
        font.family:EFont.textHanSansMedium
    }
    ChartView{
        id:rootChartview
        anchors.fill: parent
        backgroundColor: "white"
        backgroundRoundness: 4
        anchors.margins:0
        antialiasing: true
        legend.font.pixelSize: 14
        legend.font.family: EFont.textHanSansNormal
        legend.alignment:  Qt.AlignRight
        legend.markerShape: Legend.MarkerShapeCircle
        animationOptions:ChartView.AllAnimations

        //饼图中心数据
        Item{
            width: 100
            height: allpeopleText.height+allpeopleDataText.height
            x:pieMidPoint.x-width/2
            y:pieMidPoint.y-height/2
            Text {
                id:allpeopleText
                width: contentWidth
                height: contentHeight
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("总人数")
                font.pixelSize: 12
                font.family: EFont.textHanSansNormal
                color: EColor.textColor(EColor.Text_Routine)
            }
            Text {
                id:allpeopleDataText
                width: contentWidth
                height: contentHeight
                anchors.horizontalCenter: parent.horizontalCenter
                text:allPeopelCount.toString()
                font.pixelSize: 32
                font.family: EFont.textHanSansMedium
                font.bold: true
                color: EColor.textColor(EColor.Text_Main)
                anchors.bottom: parent.bottom
            }
        }
        //饼图
        PieSeries{
            id:pieSeries
            size: 0.8
            holeSize: 0.45
            onHovered: {
                slice.exploded = state
                tooltip.currText =slice.label
                tooltip.currValue = slice.value.toString()
                tooltip.currPercent = ((slice.value/allPeopelCount)*100).toFixed(0)+"%"
                tooltip.legendColor = slice.color
                var midAngle = slice.startAngle+slice.angleSpan/2
                var midRadian = midAngle*(Math.PI/180)
                var radius =rootChartview.plotArea.height/2 * (holeSize+(size-holeSize)/2)
                var midX = pieMidPoint.x +  Math.sin(midRadian)*radius
                var midY = pieMidPoint.y - Math.cos(midRadian)*radius
                tooltip.x = midX
                tooltip.y = midY
                tooltip.visible = state
            }
            PieSlice {
                id: pieSlice1;
                color: "#5b8ff9"
                explodeDistanceFactor: 0.08
            }
            PieSlice {
                id: pieSlice2;
                color: "#62daab"
                explodeDistanceFactor: 0.08
            }
            PieSlice {
                id: pieSlice3;
                color: "#657798"
                explodeDistanceFactor: 0.08
            }
            PieSlice {
                id: pieSlice4;
                color: "#eed67e"
                explodeDistanceFactor: 0.08
            }
        }


        ToolTip{
            id:tooltip
            width:140
            height: 60
            property string currText: ""
            property color legendColor: "white"
            property string currPercent: ""
            property string currValue: ""
            padding: 10
            background: Rectangle{
                anchors.fill: parent
                radius: 4
                color: "white"
                border.width: 1
                border.color: EColor.bgColor(EColor.Border_1)
            }
            contentItem: Item {
                width: parent.width
                height: parent.height
                anchors.centerIn: parent
                Text {
                    text: tooltip.currText
                    font.pixelSize: 12
                    font.family: EFont.textHanSansNormal
                    color: EColor.textColor(EColor.Text_Routine)
                    width: contentWidth
                    height: contentHeight
                }
                Text {
                    text: tooltip.currValue + "人"
                    font.pixelSize: 12
                    font.family: EFont.textHanSansMedium
                    color: EColor.textColor(EColor.Text_Main)
                    width: contentWidth
                    height: contentHeight
                    anchors.right: parent.right
                }
                Item{
                    width: parent.width
                    height: 14
                    anchors.bottom: parent.bottom
                    Rectangle{
                        id:legendRec
                        width: 8
                        height: width
                        radius: width/2
                        color: tooltip.legendColor
                        anchors.verticalCenter: parent.verticalCenter
                    }
                    Text {
                        text: qsTr("人数占比")
                        font.pixelSize: 12
                        font.family: EFont.textHanSansNormal
                        color: EColor.textColor(EColor.Text_Main)
                        width: contentWidth
                        height: contentHeight
                        anchors.left:legendRec.right
                        anchors.rightMargin: 8
                        anchors.verticalCenter: parent.verticalCenter
                    }
                    Text {
                        text: tooltip.currPercent
                        font.pixelSize: 12
                        font.family: EFont.textHanSansMedium
                        color: EColor.textColor(EColor.Text_Main)
                        width: contentWidth
                        height: contentHeight
                        anchors.right: parent.right
                        anchors.verticalCenter: parent.verticalCenter
                    }
                }
            }//end tooltip contentItem
        }//end tooltip
    }//end ChartView
}
