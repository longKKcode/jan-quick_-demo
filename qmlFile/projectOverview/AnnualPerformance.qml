import QtQuick 2.0
import QtQuick.Controls 2.12

import EUIpackage 1.0
import EControl 1.0
Rectangle{
    width: 377
    height: 369
    color: "white"
    radius: 4
    property real performanceValue: -1
    //左上角标题
    Text {
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: 24
        width: contentWidth
        height: contentHeight
        text: qsTr("年度表现")
        color: EColor.textColor(EColor.Text_Main)
        font.pixelSize: 16
        font.family:EFont.textHanSansMedium
    }

    Canvas{
        id:canvas
        width: 300
        height: 300
        anchors.left: parent.left
        anchors.leftMargin: 40
        anchors.bottom: parent.bottom
        property int lineWidth: 20
        onPaint: {
            //红色区域
            var ctx = getContext("2d");
            ctx.clearRect(0, 0,width,width);
            ctx.lineCap = "butt";
            ctx.lineWidth = lineWidth;
            ctx.strokeStyle = "#f4664a";
            ctx.save()
            ctx.arc(width/2, width/2, (canvas.width-lineWidth)/2, 143*Math.PI/180, 209*Math.PI/180, false);
            ctx.stroke();
            ctx.restore();
            //橙黄色区域
            ctx.beginPath()
            ctx.strokeStyle = "#f5db7c";
            ctx.arc(width/2, width/2, (canvas.width-lineWidth)/2, 210*Math.PI/180, 269.5*Math.PI/180, false);
            ctx.save()
            ctx.stroke();
            ctx.restore();
            ctx.closePath()
            //浅绿色区域
            ctx.beginPath()
            ctx.strokeStyle = "#a0d911";
            ctx.arc(width/2, width/2, (canvas.width-lineWidth)/2, 270.5*Math.PI/180, 330*Math.PI/180, false);
            ctx.save()
            ctx.stroke();
            ctx.restore();
            ctx.closePath()
            //深绿色区域
            ctx.beginPath()
            ctx.strokeStyle = "#30bf78";
            ctx.arc(width/2, width/2, (canvas.width-lineWidth)/2, 331*Math.PI/180, 36*Math.PI/180, false);
            ctx.save()
            ctx.stroke();
            ctx.restore();
            ctx.closePath()
            //中心圆环
            ctx.beginPath()
            ctx.strokeStyle = "#bfbfbf";
            ctx.lineWidth = 6;
            ctx.arc(width/2, width/2, 12, 0, 2*Math.PI, false);
            ctx.save()
            ctx.stroke();
            ctx.restore();
            ctx.closePath()
            //中心指针
            ctx.beginPath()
            ctx.strokeStyle = "#bfbfbf";
            ctx.translate(width/2,width/2)
            ctx.rotate(233*Math.PI/180 + (253*Math.PI/180)*performanceValue/100)
            ctx.lineTo(-3,-12)
            ctx.lineTo(0,-60)
            ctx.lineTo(3,-12)
            ctx.closePath()
            ctx.fill()
            ctx.stroke();
            ctx.restore();
        }

        Text {
            text: canvas.getText()
            width: contentWidth
            height: contentHeight
            color: canvas.getColor()
            font.pixelSize: 24
            font.family:EFont.textHanSansMedium
            anchors.bottom: tipText.top
            anchors.bottomMargin: 3
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Text {
            id: tipText
            text: qsTr("表现评价")
            width: contentWidth
            height: contentHeight
            color: EColor.textColor(EColor.Text_Secondary)
            font.pixelSize: 14
            font.family:EFont.textHanSansNormal
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottomMargin: 30
        }
        function getText(){
            //0-26为不及格
            //26-50为及格
            //50-74为良
            //74-100为优
            if(performanceValue >=0 && performanceValue <26)
                return "不及格"
            else if(performanceValue >=26 && performanceValue <50)
                return "及格"
            else if(performanceValue >=50 && performanceValue <74)
                return "良好"
            else if(performanceValue >=74 && performanceValue <=100)
                return "优秀"
            else
                return "及格"
        }
        function getColor(){
            if(performanceValue >=0 && performanceValue <26)
                return "#f4664a"
            else if(performanceValue >=26 && performanceValue <50)
                return "#f5db7c"
            else if(performanceValue >=50 && performanceValue <74)
                return "#a0d911"
            else if(performanceValue >=74 && performanceValue <=100)
                return "#30bf78"
            else
                return "f5db7c"
        }
    }//end Canvas
}
