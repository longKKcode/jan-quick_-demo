import QtQuick 2.0
import QtQuick.Controls 2.12
import QtCharts 2.13

import EUIpackage 1.0
import EControl 1.0

Rectangle{
    width: 560
    height: 376
    color: "white"
    radius: 4
    property alias stackedBarSeries: stackedBarSeries
    ChartView{
        id:rootChartview
        anchors.fill: parent
        backgroundColor: "white"
        backgroundRoundness: 4
        anchors.margins:12
        antialiasing: true
        legend.font.pixelSize: 14
        legend.font.family: EFont.textHanSansNormal
        legend.alignment:  Qt.AlignTop
        legend.markerShape: Legend.MarkerShapeCircle
        animationOptions:ChartView.SeriesAnimations
        HorizontalStackedBarSeries {
            id:stackedBarSeries
            barWidth:0.6
            onHovered:{
                tooltip.visible = status
                tooltip.bgColor = barset.color
                tooltip.model = barset.values.reverse()
                if(status){
                    barset.borderWidth = 1
                    barset.borderColor = "black"
                }
                else{
                    barset.borderWidth = 0
                    barset.borderColor = "transparent"
                }

            }
            axisX: ValueAxis{
                lineVisible: false
                labelsFont.pixelSize:12
                labelsFont.family:EFont.textHanSansNormal
                labelsColor:EColor.textColor(EColor.Text_Main)
                tickCount:6
            }

            axisY: BarCategoryAxis {
                gridVisible:false
                labelsFont.pixelSize: 12
                labelsFont.family: EFont.textHanSansNormal
                labelsColor: EColor.textColor(EColor.Text_Routine)
            }
            BarSet {
                color: "#6395fa"
            }
            BarSet {
                color: "#63daab"
            }
            BarSet {
                color: "#f4d975"
            }
        }//end HorizontalPercentBarSeries

        ToolTip{
            id:tooltip
            width:40
            height: rootChartview.plotArea.height
            padding: 0
            x:rootChartview.plotArea.x+rootChartview.plotArea.width+6
            y:rootChartview.plotArea.y
            property color bgColor: "black"
            property var model: ""
            background: Rectangle{
                anchors.fill: parent
                radius: 4
                color: tooltip.bgColor
            }
            contentItem: Item {
                anchors.fill: parent
                Column{
                    width: tooltip.width
                    Repeater{
                        id:repearter
                        model: tooltip.model
                        Item{
                            width: tooltip.width
                            height: tooltip.height/repearter.count
                            Text{
                                text: modelData.toString()+"¥"
                                color: EColor.textColor(EColor.Text_Main)
                                width: contentWidth
                                height: contentHeight
                                font.pixelSize: 14
                                font.family: EFont.textHanSansMedium
                                anchors.centerIn: parent
                            }
                        }
                    }
                }
            }//end tooltip contentItem
        }//end tooltip
    }//end ChartView
    //左上角标题
    Text {
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: 24
        width: contentWidth
        height: contentHeight
        text: qsTr("年销售统计")
        color: EColor.textColor(EColor.Text_Main)
        font.pixelSize: 16
        font.family:EFont.textHanSansMedium
    }
    //x轴标签
    Text {
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        anchors.horizontalCenter: parent.horizontalCenter
        width: contentWidth
        height: contentHeight
        text: qsTr("收入/¥")
        color: EColor.textColor(EColor.Text_Main)
        font.pixelSize: 14
        font.family:EFont.textHanSansMedium
    }
}


