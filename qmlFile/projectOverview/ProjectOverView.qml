import QtQuick 2.0
import QtQuick.Controls 2.12
import Qt.labs.settings 1.0

import EUIpackage 1.0
import EControl 1.0

import "./"

Item {
    width: 1182
    height: 769
    Flow{
        anchors.fill: parent
        spacing: 24
        //资产配置达成
        AssetPieChart{
            id:assetPieChart
            Component.onCompleted: {
                DataHandleVM.getAssetPieChartData()
            }
        }
        //年销售统计
        SellStackedBarChart{
            id:horStackedBarChart
            width: 560
            height: 376
            Component.onCompleted: {
                DataHandleVM.getSellData()
            }
        }
        //项目展示
        ProjectShowView{
            id:projectShowView
            width: 781
            height: 369
            Component.onCompleted: {
                DataHandleVM.getProjectData()
            }
            onChosedIndexChanged: {
                DataHandleVM.getProjectData()
            }
        }
        //年度表现
        AnnualPerformance{
            id:annualPerformance
            Component.onCompleted: {
                DataHandleVM.getPerformanceData()
            }
        }
    }
    Connections{
        target: DataHandleVM
        //饼图加载数据(资产数据)
        onSigGetAssetPieData:{
            if(isSuccess){
                if(dataObj["allPeople"] === undefined){
                    return
                }
                assetPieChart.allPeopelCount = dataObj["allPeople"]
                var assignInfoList = dataObj["assignInfoList"]
                for(var i = 0;i<assignInfoList.length;i++){
                    var obj = assignInfoList[i]
                    assetPieChart.pieSeries.at(i).label = obj.projectName
                    assetPieChart.pieSeries.at(i).value = obj.assignValue
                }
            }
            else{
                eToast.showToast("饼图本地数据加载失败",EToast.StyleEnum.ERROR)
            }
        }

        //水平堆叠柱状图加载数据(年销售统计)
        onSigGetSellData:{
            if(isSuccess){
                var yearArr = dataObj["yearArr"]
                var teatDataList = dataObj["teamDataList"]
                var maxValueOfYear = dataObj["maxValueOfYear"]
                if(yearArr === undefined || teatDataList === undefined){
                    return
                }
                horStackedBarChart.stackedBarSeries.axisX.min = 0
                horStackedBarChart.stackedBarSeries.axisX.max = maxValueOfYear
                horStackedBarChart.stackedBarSeries.axisY.categories= yearArr
                for(var i = 0;i<teatDataList.length;i++){
                    var teamObj = teatDataList[i]
                    horStackedBarChart.stackedBarSeries.at(i).label = teamObj.teamName
                    horStackedBarChart.stackedBarSeries.at(i).values = teamObj.valueList
                }
            }
            else{
                eToast.showToast("堆叠柱状图加载数据失败",EToast.StyleEnum.ERROR)
            }
        }
        //项目展示区域加载数据
        onSigGetProjectData:{
            if(isSuccess){
                projectShowView.viewModel.clear()
                var proJectList
                switch (projectShowView.chosedIndex){
                case 0://所有项目
                    proJectList = dataObj["allProList"]
                    break
                case 1://当前项目
                    proJectList = dataObj["processingProList"]
                    break
                case 2://已完成项目
                    proJectList = dataObj["completedProList"]
                    break
                default:
                    proJectList = dataObj["allProList"]
                    break
                }
                for(let i =0;i<proJectList.length;++i){
                    projectShowView.viewModel.append(proJectList[i])
                }
            }
            else{
                eToast.showToast("项目数据加载失败",EToast.StyleEnum.ERROR)
            }
        }

        onSigGetPerformanceData:{
            if(isSuccess){
                var performanceScore = dataObj["performanceScore"]
                annualPerformance.performanceValue = performanceScore
            }
            else{
                eToast.showToast("年度表现数据加载失败",EToast.StyleEnum.ERROR)
            }
        }
    }//end Connections
}
