import QtQuick 2.0
import QtQuick.Controls 2.12
import QtCharts 2.13

import EUIpackage 1.0
import EControl 1.0

Rectangle{
    width: 781
    height: 369
    color: "white"
    radius: 4
    property alias viewModel: girdViewModel
    property alias chosedIndex: combobox.currentIndex
    Item{
        id:headerItem
        width: parent.width
        height: 56
        Text {
            text: qsTr("项目展示")
            font.pixelSize: 16
            width: contentWidth
            height: contentHeight
            anchors.left: parent.left
            anchors.leftMargin: 24
            anchors.verticalCenter: parent.verticalCenter
            color: EColor.textColor(EColor.Text_Main)
            font.family:EFont.textHanSansMedium
        }
        ECombobox{
            id:combobox
            width: 140
            model: ["所有项目","进行中的项目","已完成项目"]
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 16
            currentIndex: 0
        }
        Rectangle{
            width: parent.width
            height: 1
            anchors.bottom: parent.bottom
            color: EColor.bgColor(EColor.Border_1)
        }
    }
    GridView{
        id:gridview
        width: parent.width
        height: parent.height-headerItem.height
        anchors.top: headerItem.bottom
        model:girdViewModel
        clip: true
        cellWidth: gridview.width/3
        cellHeight:gridview.height/2
        delegate: ItemDelegate{
            width: gridview.width/3
            height: gridview.height/2

            background: Item{
                anchors.fill: parent
            }
            onClicked: {
                eToast.showToast("暂无项目详情页面",EToast.StyleEnum.WARNING)
            }

            contentItem: Item{
                anchors.fill: parent
                anchors.margins:18
                //项目名称
                Text{
                    id:projectNameText
                    width: contentWidth
                    height: contentHeight
                    text: projectName
                    font.pixelSize: 16
                    font.family: EFont.textHanSansMedium
                    color:hovered? EColor.mainColor(EColor.MColor_1):"black"
                }
                //项目简介
                Text{
                    text: briefIntroduction
                    wrapMode: Text.Wrap
                    width: parent.width
                    height: parent.height-projectNameText.height-projectTeamText.height-24
                    anchors.top:projectNameText.bottom
                    anchors.topMargin: 12
                    font.pixelSize: 12
                    font.family: EFont.textHanSansNormal
                    color:EColor.textColor(EColor.Text_Main)
                }
                //团队名
                Text {
                    id: projectTeamText
                    text: teamName
                    font.pixelSize: 14
                    font.family: EFont.textHanSansNormal
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    width: parent.width/2
                    height: contentHeight
                    color:EColor.mainColor(EColor.MColor_0)
                }
                //项目开始时间
                Text {
                    id: startTimeText
                    text: {
                        var startDate = new Date(Number(startTime)*1000)
                        var Y = startDate.getFullYear() + '-';
                        var M= (startDate.getMonth()+1 < 10 ? '0'+(startDate.getMonth()+1) : startDate.getMonth()+1) + '-';
                        var D = startDate.getDate();
                        return (Y+M+D)
                    }
                    font.pixelSize: 14
                    font.family: EFont.textHanSansNormal
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    width: contentWidth
                    height: contentHeight
                    color:EColor.textColor(EColor.Text_Main)
                }
            }//end contentItem
            //底部分割线
            Rectangle{
                anchors.bottom: parent.bottom
                width: parent.width
                height: 1
                color: EColor.bgColor(EColor.Border_1)
            }
            //右侧分割线
            Rectangle{
                anchors.right: parent.right
                visible: (index+1)%3 !== 0
                width: 1
                height: parent.height
                color: EColor.bgColor(EColor.Border_1)
            }
        }//end delegate
    }//end GridView
    ListModel{
        id:girdViewModel
    }
}
