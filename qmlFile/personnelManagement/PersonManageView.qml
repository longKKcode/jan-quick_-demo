import QtQuick 2.13
import QtQuick.Controls 1.4
import QtQuick.Controls 2.12
import Qt.labs.settings 1.0

import EUIpackage 1.0
import EControl 1.0

import "./"
import "../demoControl"

Rectangle{
    color: "white"
    radius: 4

    EBaseTableView{
        id:tableview
        anchors.fill: parent
        model:etablemodel
        TableViewColumn {
            role: "name";
            title: "姓名";
            width: (parent.width-50)/5
        }
        TableViewColumn {
            role: "country";
            title: "国家";
            width: (parent.width-50)/5
        }
        TableViewColumn {
            role: "city";
            title: "城市";
            width: (parent.width-50)/5
        }
        TableViewColumn {
            role: "birthday";
            title: "出生日期";
            width: (parent.width-50)/5
        }
        TableViewColumn {
            role: "contact";
            title: "联系方式";
            width: (parent.width-50)/5
        }

        MouseArea{
            anchors.fill: parent
            acceptedButtons: Qt.RightButton
            onClicked: {
                tableview.currSelectedRow = tableview.rowAt(mouseX,mouseY)
                menu.popup(mouseX,mouseY)
            }
        }
        CusMenu{
            id:menu
            Action{
                text: "修改员工信息"
                icon.name: "\uf044"
                onTriggered: {
                    //打开dialog清空输入
                    let childrenList = inputColumn.children
                    let name = DataHandleVM.getRowData(etablemodel,tableview.currSelectedRow,"name")
                    for(var i = 0;i<etablemodel.roles.length;++i){
                        let value = DataHandleVM.getRowData(etablemodel,tableview.currSelectedRow,etablemodel.roles[i])
                        childrenList[i].inputText =value.toString()
                    }
                    inputdialog.currSelectedRow = tableview.currSelectedRow
                    inputdialog.isNew = false
                    inputdialog.open()
                }
            }
            Action{
                text: "删除当前员工"
                icon.name: "\uf056"
                enabled: tableview.currSelectedRow >=0
                onTriggered: {
                    DataHandleVM.removeRow(etablemodel,tableview.currSelectedRow)
                }
            }
            Action{
                text: "添加新员工"
                icon.name: "\uf055"
                onTriggered: {
                    //打开dialog清空输入
                    var childrenList = inputColumn.children
                    for(var i = 0;i<childrenList.length;++i){
                        childrenList[i].inputText =""
                    }
                    inputdialog.isNew = true
                    inputdialog.open()
                }
            }
            onVisibleChanged: {
                if(!visible)
                     tableview.currSelectedRow = -1
            }
        }
    }

    Dialog{
        id:inputdialog
        anchors.centerIn: parent
        width: parent.width/2.5
        height: parent.height/4*3
        modal: true
        closePolicy: Popup.NoAutoClose
        property bool isNew: false
        property int currSelectedRow: -1
        background: Rectangle{
            radius: 4
            anchors.fill: parent
            border.width: 1
            border.color: EColor.borderColor(EColor.Border_1)
        }
        contentItem: Item{
            anchors.fill: parent
            Text {
                id: titleText
                text: inputdialog.isNew? "添加新成员" : "修改成员信息"
                anchors.top: parent.top
                anchors.topMargin: 12
                anchors.horizontalCenter: parent.horizontalCenter
                font.family: EFont.textHanSansMedium
                font.pixelSize: 26
                color: EColor.textColor(EColor.Text_Main)
                width: contentWidth
                height: contentHeight
            }
            Column{
                id:inputColumn
                width:  parent.width-40
                anchors.top: titleText.bottom
                anchors.topMargin: 22
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: 22
                CusTipInput{
                    width: parent.width
                    height: 40
                    tiptext: "姓        名"
                    placeholderText:"请输入姓名"
                }
                CusTipInput{
                    width: parent.width
                    height: 40
                    tiptext: "国        家"
                    placeholderText:"请输入国家名"
                }
                CusTipInput{
                    width: parent.width
                    height: 40
                    tiptext: "城        市"
                    placeholderText:"请输入城市名"
                }

                CusTipInput{
                    width: parent.width
                    height: 40
                    tiptext: "出生日期"
                    placeholderText:"请输入出生日期"
                }
                CusTipInput{
                    width: parent.width
                    height: 40
                    tiptext: "联系方式"
                    placeholderText:"请输入联系方式"
                }
            }//end columm
            EBaseBtn{
                width: 100
                height: 40
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.margins: 20
                cusText:"取消"
                onClicked: {
                    inputdialog.close()
                }
            }
            EMainBtn{
                width: 100
                height: 40
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.margins: 20
                cusText:"确定"
                onClicked: {
                    var childrenList = inputColumn.children
                    if(childrenList[0].inputText === ""){
                        dialogToast.showToast("姓名不能为空",EToast.StyleEnum.ERROR)
                        return
                    }

                    if(inputdialog.isNew){
                        var dataList = []
                        for(var i = 0;i<childrenList.length;++i){
                            dataList.push(childrenList[i].inputText)
                        }
                        DataHandleVM.insertNewRow(etablemodel,dataList)
                    }
                    else{
                        var dataObj = {}
                        for(i = 0;i<etablemodel.roles.length;++i){
                            let role = etablemodel.roles[i]
                            dataObj[role] = childrenList[i].inputText.toString()
                        }
                        DataHandleVM.setRowData(etablemodel,inputdialog.currSelectedRow,dataObj)
                    }

                    inputdialog.close()
                    inputdialog.currSelectedRow =-1
                }
            }
            EToast{
                id:dialogToast
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
            }
        }//end Dialog.contenItem
    }//end Dialog

    ETableModel{
        id:etablemodel
        roles:["name","country","city","birthday","contact"]
        Component.onCompleted: {
            DataHandleVM.loadPeopelModelData(etablemodel)
        }
    }
}
