import QtQuick 2.0
import QtQuick.Controls 2.12

import EUIpackage 1.0
import EControl 1.0
Item {
    property alias model: repeater.model
    Row{
        spacing: 8
        Repeater{
            id:repeater
            Item {
                width:itemText.width+separaText.width+8
                height: itemText.height
                anchors.verticalCenter: parent.verticalCenter
                Text {
                    id: itemText
                    text: modelData
                    width:contentWidth
                    height: contentHeight
                    font.family:EFont.textHanSansNormal
                    font.pixelSize: 12
                    anchors.verticalCenter: parent.verticalCenter
                    color: index === repeater.count-1? EColor.textColor(EColor.Text_Main) : EColor.textColor(EColor.Text_Secondary)
                }
                Text {
                    id:separaText
                    text: "/"
                    width:contentWidth
                    height: contentHeight
                    anchors.left: itemText.right
                    anchors.leftMargin: 8
                    font.family:EFont.textHanSansNormal
                    font.pixelSize: 12
                    anchors.verticalCenter: parent.verticalCenter
                    color:  EColor.textColor(EColor.Text_Secondary)
                    visible:index !== repeater.count-1
                }
            }
        }
    }
}
