import QtQuick 2.0
import QtQuick.Controls 2.12

import EUIpackage 1.0
import EControl 1.0

Item{
    implicitHeight:40
    implicitWidth: 70
    property string tiptext: "提示"
    property alias inputText: input.text
    property alias placeholderText: input.placeholderText
    Text {
        id: tipText
        text: tiptext+":"
        font.family: EFont.textHanSansNormal
        font.pixelSize: 16
        color: EColor.textColor(EColor.Text_Main)
        width: contentWidth
        height: contentHeight
        anchors.verticalCenter: parent.verticalCenter
    }
    EBaseInput{
        id:input
        width: parent.width-tipText.width-12
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: tipText.right
        anchors.leftMargin: 12
    }
}
