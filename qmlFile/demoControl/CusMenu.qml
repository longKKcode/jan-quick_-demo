import QtQuick 2.12
import QtQuick.Controls 2.12

import EUIpackage 1.0
import EControl 1.0

Menu {
    id: menu
    topPadding: 8
    bottomPadding: 8
    leftPadding: 8
    rightPadding: 8
    spacing: 8
    delegate: MenuItem {
        id: menuItem
        implicitWidth: 180
        implicitHeight: enabled?32:0
        leftPadding: 16
        visible: enabled
        indicator: Text {
            id: addImg
            text: menuItem.icon.name
            width: 20
            height: 20
            color: EColor.textColor(EColor.Text_Main)
            font.family: EFont.iconRegular
            font.pixelSize: 14
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            leftPadding: 10
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        contentItem: Text {
            leftPadding: 12
            rightPadding: menuItem.arrow.width
            text: menuItem.text
            opacity: enabled ? 1.0 : 0.3
            color: EColor.textColor(EColor.Text_Routine)
            font.family: EFont.textHanSansNormal
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
            font.pixelSize: 14
        }

        background: Rectangle {
            implicitWidth: 180
            implicitHeight: 32
            opacity: enabled ? 1 : 0.3
            radius: 4
            color: menuItem.highlighted ?EColor.mainColor(EColor.MColor_5) :"white"
        }
    }

    background: Rectangle {
        implicitWidth: 180
        implicitHeight: 48
        color: "white"
        border.width: 1
        border.color: EColor.borderColor(EColor.Border_2)
        radius: 4
    }
}
