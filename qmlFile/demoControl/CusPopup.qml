import QtQuick 2.0
import QtQuick.Controls 2.12

import EUIpackage 1.0
import EControl 1.0

Popup{
    id:cusPopup
    y:parent.height+4
    width: parent.width
    property var model: ""
    property string textRole: ""
    property alias currentIndex: dataListview.currentIndex
    property real cusItemHeight: 40
    height: dataListview.count >= 5? (cusItemHeight*5+2) : (cusItemHeight*dataListview.count+2)
    background: Rectangle {
        anchors.fill: parent
        radius: 4
        border.width: 1
        border.color: EColor.borderColor(EColor.Border_1)
    }
    contentItem: ListView{
        id:dataListview
        anchors.fill: parent
        anchors.margins: 1
        clip: true
        model: cusPopup.model
        delegate: ItemDelegate{
            width: parent.width
            height: cusItemHeight
            contentItem: Text {
                id:dataText
                text: cusPopup.textRole === "" ? modelData : cusPopup.model.get(index)[cusPopup.textRole]
                width: parent.width-20
                height: parent.height
                anchors.left: parent.left
                anchors.leftMargin: 10
                verticalAlignment: Text.AlignVCenter
                color:dataListview.currentIndex === index? EColor.mainColor(EColor.MColor_1) : EColor.textColor(EColor.Text_Routine)
                font.pixelSize:14
                font.family: EFont.textHanSansMedium
                elide: Text.ElideRight
            }

            background: Rectangle{
                anchors.fill: parent
                radius: 4
                color: hovered? EColor.bgColor(EColor.BGColor_2): "white"
            }
            onClicked: {
                dataListview.currentIndex = index
                cusPopup.visible = false
            }
        }
    }
}
