import QtQuick 2.0
import QtQuick.Controls 2.12
import Qt.labs.settings 1.0

import EUIpackage 1.0
import EControl 1.0

Item {
    width: 1440
    height: 900
    signal sigSuccessLogin(var userName)
    Component.onCompleted: {
        if(mySettings.autoLogin){
            autoLoginTimer.start()
        }
    }

    //自动登录延时计时器 300ms后自动登录
    Timer{
        id:autoLoginTimer
        interval: 300
        running: false
        repeat: false
        onTriggered: {
            login()
        }
    }

    //注册表，记住密码使用
    Settings{
        id:mySettings
        category:"LoginSetting"
        property bool autoLogin: autoLoginBox.checked
        property string account: accountInput.text
        property string password:passwordInput.text
    }

    //左侧简介区域
    Rectangle{
        width: parent.width/2
        height: parent.height
        color: EColor.bgColor(EColor.BGColor_1)
        Item{
            width: parent.width
            height: logoText.height+introduceText.height+10
            anchors.centerIn: parent
            Text {
                id: logoText
                width: contentWidth
                height: contentHeight
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: 38
                font.family: EFont.textHanSansMedium
                text: qsTr("JanQuick")
                color: EColor.mainColor(EColor.MColor_1)
            }
            Text {
                id:introduceText
                anchors.top: logoText.bottom
                anchors.topMargin: 10
                width: contentWidth
                height: contentHeight
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: 21
                font.family: EFont.textHanSansNormal
                text: qsTr("这是一个用JanQuick_Eui控件库写的Demo")
                color: EColor.textColor(EColor.Text_Routine)
            }
        }
    }

    //右侧登录区域
    Rectangle{
        width: parent.width/2
        height: parent.height
        anchors.right: parent.right
        color: "white"
        Item {
            width: 250
            height: 320
            anchors.centerIn: parent
            EBaseTabBar{
                id:tabBar
                height: 32
                width: parent.width
                cusModel: ["账号密码登录","手机登录"]
                cusBotPlaceLineCor:"white"
                cusTextSize:18
                cusSpacing: 34
                onCurrentIndexChanged: {
                    if(currentIndex ===1){
                        eToast.showToast("暂无手机登录功能",EToast.StyleEnum.WARNING)
                    }
                }
            }
            Column{
                width: parent.width
                anchors.top: tabBar.bottom
                anchors.topMargin: 30
                spacing: 20
                EBaseInput{
                    id:accountInput
                    width: parent.width
                    height:40
                    placeholderText:"请输入账号"
                    text:mySettings.account
                }
                EPasswordInput{
                    id:passwordInput
                    width: parent.width
                    height:40
                    placeholderText:"请输入密码"
                    text:mySettings.password
                    onAccepted: {
                        login()
                    }
                }

                Item{
                    height: 22
                    width: parent.width
                    ECheckbox{
                        id:autoLoginBox
                        text: "自动登录"
                        cusIconSize:14
                        height: parent.height
                        checked: mySettings.autoLogin
                    }
                    Text {
                        width: contentWidth
                        height: contentHeight
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        font.pixelSize: 14
                        font.family: EFont.textHanSansNormal
                        color: EColor.mainColor(EColor.MColor_1)
                        text: "忘记密码"
                        MouseArea{
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: {
                                eToast.showToast("暂无忘记密码功能",EToast.StyleEnum.WARNING)
                            }
                            onEntered: {
                                cursorShape = Qt.PointingHandCursor
                            }
                            onExited: {
                                cursorShape = Qt.ArrowCursor
                            }
                        }
                    }
                }
                EMainBtn{
                    width: parent.width
                    height: 40
                    cusText: "立即登录"
                    onClicked: {
                        login()
                    }
                }
                Item{
                    height: 22
                    width: parent.width
                    Text {
                        id: otherText
                        width: contentWidth
                        height: contentHeight
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        font.pixelSize: 14
                        font.family: EFont.textHanSansNormal
                        color: EColor.textColor(EColor.Text_Routine)
                        text: "其他登录方式"
                    }
                    Text {
                        width: contentWidth
                        height: contentHeight
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        font.pixelSize: 14
                        font.family: EFont.textHanSansNormal
                        color: EColor.mainColor(EColor.MColor_1)
                        text: "账号注册"
                        MouseArea{
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: {
                                var obj = {account:"test",password:"123456",url:""}
                                DataHandleVM.registerAccount(obj)
                                eToast.showToast("暂无注册功能",EToast.StyleEnum.WARNING)
                            }
                            onEntered: {
                                cursorShape = Qt.PointingHandCursor
                            }
                            onExited: {
                                cursorShape = Qt.ArrowCursor
                            }
                        }
                    }
                }
            }//end Column
        }//end contentitem
    }//end rightbg

    //登录函数
    function login(){
        if(DataHandleVM.checkAccount(accountInput.text,passwordInput.text)){
            sigSuccessLogin(accountInput.text)
        }
        else{
            passwordInput.cusIsError = true
            eToast.showToast("密码或密码错误",EToast.StyleEnum.ERROR)
        }
    }
}
