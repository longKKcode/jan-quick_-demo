import QtQuick 2.0
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.13
import Qt.labs.settings 1.0

import EUIpackage 1.0
import EControl 1.0

import "./"
import "./demoControl"
import "./projectOverview"

Page {
    signal sigSignout

    //左上角logo区域
    Rectangle{
        id:mainIconArea
        width: 210
        height: 56
        color: "#334154"
        Text {
            text: qsTr("JanQuick")
            width: contentWidth
            height: contentHeight
            font.pixelSize: 28
            anchors.centerIn: parent
            font.family: EFont.textHanSansMedium
            color: EColor.mainColor(EColor.MColor_1)
        }
    }

    //侧边导航栏背景
    Rectangle{
        id:listviewBg
        width: 210
        height: parent.height-mainIconArea.height
        anchors.top: mainIconArea.bottom
        color: "#222d3c"
        visible: true
        NavExpListView{
            id:navigationBar
            anchors.fill: parent
            clip: true
            Component.onCompleted: {
                initNav()
                initTimer.start()
            }
            onSigChildClicked: {
                breadcrumb.model = getBreadcrumbModel()
            }

        }
    }//end listviewBg

    //顶部菜单栏背景
    Rectangle{
        id:menuArea
        width: parent.width-listviewBg.width
        height: 56
        anchors.left: listviewBg.right
        anchors.top: parent.top
        color: "white"

        //右上角菜单行排列
        Row{
            height: parent.height
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            spacing: 10
            //搜索按键
            EIconBaseBtn{
                anchors.verticalCenter: parent.verticalCenter
                cusBorderVis: false
                cusIconSize:18
                onClicked: {
                    eToast.showToast("暂无搜索功能",EToast.StyleEnum.WARNING)
                }
            }

            //消息提醒按键
            EIconBaseBtn{
                cusIcon: "\uf0f3"
                anchors.verticalCenter: parent.verticalCenter
                cusBorderVis: false
                cusIconSize:18
                onClicked: {
                    eToast.showToast("暂无消息展示功能",EToast.StyleEnum.WARNING)
                }
            }

            //用户信息
            Rectangle{
                height: 40
                width: avatarBg.width+8+userNameText.contentWidth
                anchors.verticalCenter: parent.verticalCenter
                color: "transparent"
                radius: 4
                Rectangle{
                    id:avatarBg
                    width: 24
                    height: width
                    clip: true
                    radius: width/2
                    color: "lightblue"
                    anchors.verticalCenter: parent.verticalCenter
                    Image {
                        id: avatar
                        anchors.fill: parent
                        clip: true
                        visible: false
                        fillMode: Image.PreserveAspectFit
                        source: ""
                        antialiasing: true
                    }
                    Rectangle{
                        id:avatarMask
                        anchors.fill: parent
                        radius: parent.width/2
                        visible: false
                        antialiasing: true
                    }
                    OpacityMask {
                        anchors.fill: avatarBg
                        source: avatar
                        maskSource: avatarMask
                        visible: true
                        antialiasing: true
                    }
                }
                Text {
                    id:userNameText
                    width: contentWidth
                    height: contentHeight
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: avatarBg.right
                    anchors.leftMargin: 8
                    text: qsTr("")
                    font.pixelSize: 18
                    font.family: EFont.textHanSansNormal
                    color: EColor.textColor(EColor.Text_Main)
                }
                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        if(!userPopup.visible)
                            userPopup.visible = true
                    }
                    onEntered: {
                        parent.color = EColor.mainColor(EColor.MColor_10)
                    }
                    onExited: {
                        parent.color = "transparent"
                    }
                }
                //用户信息弹出框
                CusPopup{
                    id:userPopup
                    model: ["个人信息","退出登录"]
                    y:parent.height
                    currentIndex: -1
                    onCurrentIndexChanged: {
                        switch (currentIndex){
                        case 0:
                            eToast.showToast("暂无个人信息查看功能",EToast.StyleEnum.WARNING)
                            break
                        case 1:
                            sigSignout()
                            break
                        }
                        currentIndex =-1
                    }
                }
            }//end  Rectangle 用户信息背景

            //切换语言按键
            EIconBaseBtn{
                anchors.verticalCenter: parent.verticalCenter
                cusBorderVis: false
                cusIcon: "\uf0ac"
                cusIconSize:18
                onClicked: {
                    languagePopup.visible = true
                }
                CusPopup{
                    id:languagePopup
                    model: ["中文","English"]
                    y:parent.height-2
                    currentIndex: -1
                    x:parent.width-width-2
                    width:76
                    onCurrentIndexChanged: {
                        switch (currentIndex){
                        case 0:
                            eToast.showToast("暂无切换语言功能",EToast.StyleEnum.WARNING)
                            break
                        case 1:
                            eToast.showToast("暂无切换语言功能",EToast.StyleEnum.WARNING)
                            break
                        }
                        currentIndex =-1
                    }
                }
            }
        }//end Row
    }//end menuArea

    DropShadow{//分界线阴影
        anchors.fill: menuArea
        source: menuArea
        color: "#dde1e5"
        radius: 4
        samples: 8
        horizontalOffset: 1
        verticalOffset: 6
        spread: 0
        z:1
    }

    //内容显示区域背景
    Rectangle{
        id:contentBg
        anchors.left: menuArea.left
        anchors.top: menuArea.bottom
        width: menuArea.width
        height: parent.height-menuArea.height
        color: EColor.bgColor(EColor.BGColor_1)
        Breadcrumb{
            id:breadcrumb
            anchors.left: parent.left
            anchors.leftMargin: 24
            anchors.top: parent.top
            anchors.topMargin: 16
        }

        Loader{
            width: 1182
            height: 769
            anchors.left: breadcrumb.left
            anchors.top:breadcrumb.bottom
            anchors.topMargin: 48
            source: getLoaderSource()
        }

    }

    Timer{
        id:initTimer
        interval: 100
        running: false
        repeat: false
        onTriggered: {
            breadcrumb.model = getBreadcrumbModel()
        }
    }

    //加载侧边导航栏
    function initNav(){
        var datalist = [{paretObjName:"首页",
                         childenList:[{childObjName:"项目总览",parentIndex:0},
                                      {childObjName:"人员管理",parentIndex:0}]},
                ]
        for(var i = 0;i<datalist.length;++i){
            navigationBar.model.append(datalist[i])
        }
    }

    //获取标题面包屑的model
    function getBreadcrumbModel(){
        var currModelData = navigationBar.model.get(navigationBar.currentIndex)
        var firstLevel = currModelData.paretObjName
        var childenListModel = currModelData.childenList
        var secendLevel = childenListModel.get(navigationBar.currChildIndex).childObjName
        var retArr = [firstLevel,secendLevel]
        return retArr
    }

    //登录加载用户信息
    function loginUser(userName,headPortraitUrl){
        userNameText.text = userName
        avatar.source = headPortraitUrl
    }

    //根据导航栏加载不同页面
    function getLoaderSource(){
        if(navigationBar.currentIndex ===0 && navigationBar.currChildIndex ===0)
            return "qrc:/qmlFile/projectOverview/ProjectOverView.qml"
        else if(navigationBar.currentIndex ===0 && navigationBar.currChildIndex ===1)
            return "qrc:/qmlFile/personnelManagement/PersonManageView.qml"
        else
            return ""
    }
}
