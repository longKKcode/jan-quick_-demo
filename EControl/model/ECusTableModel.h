#ifndef ECUSTABLEMODEL_H
#define ECUSTABLEMODEL_H
#include <QObject>
#include <QAbstractTableModel>

class ECusTableModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_PROPERTY(QStringList roles READ roles WRITE setRoles)
public:
    ECusTableModel(QObject *parent = nullptr);
    ~ECusTableModel() override;

    //继承基类的函数
    QVariant data(const QModelIndex &index, int role) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QHash<int, QByteArray> roleNames() const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    //根据行和roleNmae获取数据
    QVariant rowData(const int row,const QString roleName);
    //根据行和roleName修改数据
    bool setRowData(const int row,const QString roleName,const QVariant newValue);
    //增加行到末尾
    void insertNewRowData(QVariantList dataList);
    //删除行
    void removeRowData(int row);

    QStringList roles() const;
    void setRoles(const QStringList roles);

    void loadData(QList<QVariantList> data);
signals:
private:
    QList<QVariantList> m_data;
    QStringList m_roles;
};

#endif // ECUSTABLEMODEL_H
