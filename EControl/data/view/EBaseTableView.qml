import QtQuick 2.13
import QtQuick.Controls 1.4
import QtQuick.Controls 2.12
import Qt.labs.settings 1.0

import EUIpackage 1.0
import EControl 1.0
TableView{
    id:tableview
    frameVisible:false
    anchors.margins: 24
    signal rowHoveredChanged(var hovered,var row)
    property int currSelectedRow: -1
    headerDelegate: Rectangle{
        width: parent.width
        height: 50
        color: "white"
        Rectangle{
            width: 1
            height: 25
            color: EColor.borderColor(EColor.Border_3)
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
        }
        Rectangle{
            width: parent.width
            height: 1
            anchors.bottom: parent.bottom
            color: EColor.borderColor(EColor.Border_3)
        }
        Text {
            text: styleData.value
            color: EColor.textColor(EColor.Text_Main)
            font.pixelSize: 16
            width: parent.width
            height: contentHeight
            elide:Text.ElideRight
            anchors.left: parent.left
            anchors.leftMargin: 2
            anchors.verticalCenter: parent.verticalCenter
            font.family: EFont.textHanSansNormal
        }
    }

    rowDelegate:Rectangle{
        id:rowbg
        width: parent.width
        height: 48
        color: "transparent"
        border.width: 0
        property int currSelectedRow: tableview.currSelectedRow
        Rectangle{
            width: parent.width
            height: 1
            color: EColor.borderColor(EColor.Border_3)
            anchors.bottom: parent.bottom

        }
        onCurrSelectedRowChanged: {
            if(currSelectedRow ===styleData.row)
                rowbg.color = EColor.bgColor(EColor.BGColor_2)
            else
                rowbg.color = "transparent"
        }

        Connections{
            target: tableview
            onRowHoveredChanged:{
                if(row ===styleData.row && currSelectedRow !==styleData.row){
                    if(hovered)
                        rowbg.color = EColor.bgColor(EColor.BGColor_2)
                    else
                        rowbg.color = "transparent"
                }
            }
        }
    }

    itemDelegate: Item{
        id:itemdel
        width: parent.width
        height: 48
        clip: true
        property bool hovered: false
        onHoveredChanged: {
            tableview.rowHoveredChanged(hovered,styleData.row)
        }
        MouseArea{
            anchors.fill: parent
            hoverEnabled: true
            onEntered: itemdel.hovered= true
            onExited: itemdel.hovered = false
        }
        Text {
            text: styleData.value
            color: EColor.textColor(EColor.Text_Routine)
            font.pixelSize: 14
            width: parent.width
            height: contentHeight
            elide:Text.ElideRight
            anchors.verticalCenter: parent.verticalCenter
            font.family: EFont.textHanSansNormal
        }

    }
}
