import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12

import EUIpackage 1.0
import EControl 1.0
import "./qmlFile"

Window {
    id:rootWin
    visible: true
    width: 1440
    height: 900
    maximumWidth: 1440
    maximumHeight: 900
    minimumWidth: 1440
    minimumHeight: 900
    title: qsTr("JanQuick")
    flags: Qt.Window

    property int rootWinX //用来存储主窗口x坐标
    property int rootWinY //存储窗口y坐标
    property int xMouse //存储鼠标x坐标
    property int yMouse //存储鼠标y坐标

    property string currentUser: ""
    StackView{
        id:stackView
        width: parent.width
        height: parent.height
        initialItem:loginPageCom
        background: Rectangle{
            anchors.fill: parent
            radius: 8
        }
    }

    //登录页面组件
    Component{
        id:loginPageCom
        LoginPage{
            id:loginPage
            onSigSuccessLogin:{
                currentUser = userName
                stackView.push(systemPageCom)
            }
        }
    }

    //系统页面组件
    Component{
        id:systemPageCom
        SystemPage{
            id:systemPage
            onSigSignout: {
                stackView.pop()
            }
            Component.onCompleted: {
                var headPortraitUrl = DataHandleVM.getInfoByUser(currentUser,"avatarUrl").toString()
                loginUser(currentUser,headPortraitUrl)
            }
        }
    }

    //全局toast提示
    EToast{
        id:eToast
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
