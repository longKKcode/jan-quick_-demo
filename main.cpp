#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QApplication>

#include <EControl/general/EClipBoard.h>
#include <EControl/general/MyColor.h>
#include <EControl/general/MyFont.h>
#include <EControl/model/ECusTableModel.h>

#include "./sqlite/sqlitehandle.h"
#include "./datahandle/datahandlevm.h"
int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);

    QApplication::setApplicationName("JanQuick_Demo");
    QApplication::setOrganizationName("JanQuick");

    QQmlApplicationEngine engine;


    //添加eui控件
    engine.addImportPath(QStringLiteral("qrc:/"));
    //注册字体单例
    //字体图标查询网址 https://fontawesome.com/v5/search
    MyFont::getInstance();
    //注册颜色单例
    MyColor::getInstance();
    //注册剪切板类
    qmlRegisterType<EClipBoard>("EUIpackage",1,0,"EClipboard");
    qmlRegisterType<ECusTableModel>("EUIpackage",1,0,"ETableModel");

    //将实例暴露给qml
    engine.rootContext()->setContextProperty("DataHandleVM",new DataHandleVM(&engine));

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
