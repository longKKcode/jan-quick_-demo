#include "datahandlevm.h"
#include <QDebug>
#include <QFile>
#include <QIODevice>
#include <QJsonDocument>
#include <QtConcurrent>
#include <QVector>
DataHandleVM::DataHandleVM(QObject *parent) : QObject(parent)
{
    m_sqlHandle = new SqliteHandle();
}

DataHandleVM::~DataHandleVM()
{
    m_sqlHandle->deleteLater();
}

bool DataHandleVM::registerAccount(QJsonObject userInfo)
{
    qDebug()<<__FUNCTION__<<"userInfo:"<<userInfo;
    return  true;
}

bool DataHandleVM::checkAccount(QString account, QString password)
{
    bool isAccountExist = m_sqlHandle->checkExist("userTable","account",account);
    if(isAccountExist){
        QString retPass = m_sqlHandle->getValue("userTable","account",account,"password").toString();
        if(password ==retPass)
            return  true;
        else{
            qDebug()<<"密码错误";
            return  false;
        }
    }
    else{
        qDebug()<<"账户不存在";
        return false;
    }
}

QVariant DataHandleVM::getInfoByUser(QString account, QString infoKey)
{
    bool isAccountExist = m_sqlHandle->checkExist("userTable","account",account);
    if(isAccountExist){
        QVariant retValue = m_sqlHandle->getValue("userTable","account",account,infoKey);
        return retValue;
    }
    else{
        qDebug()<<"账户不存在";
        return QVariant();
    }
}

void DataHandleVM::getAssetPieChartData()
{
    QtConcurrent::run([=](){
        QFile localDataFile("./chartLocalData.json");
        if(!localDataFile.open(QIODevice::ReadOnly)){
            qDebug()<<"打开本地数据文件失败";
            emit sigGetAssetPieData(false,QJsonObject());
        }
        QJsonDocument datDoc = QJsonDocument::fromJson(localDataFile.readAll());
        QJsonObject dataObj = datDoc.object().value("assetPie").toObject();
        //qDebug()<<"dataObj>>"<<dataObj;
        emit sigGetAssetPieData(true,dataObj);
    });
}

void DataHandleVM::getSellData()
{
    QtConcurrent::run([=](){
        QFile localDataFile("./chartLocalData.json");
        if(!localDataFile.open(QIODevice::ReadOnly)){
            qDebug()<<"打开本地数据文件失败";
            emit sigGetSellData(false,QJsonObject());
        }
        QJsonDocument datDoc = QJsonDocument::fromJson(localDataFile.readAll());
        QJsonObject dataObj = datDoc.object().value("salesStatistics").toObject();

        int starYear = dataObj.value("startYear").toInt();
        int endYear = dataObj.value("endYear").toInt();
        QJsonArray yearArr;
        for(int i = starYear;i<=endYear;i++){
            yearArr.append(i);
        }
        dataObj.insert("yearArr",yearArr);

        QJsonArray yearDataArr = dataObj.value("yearDataList").toArray();
        int maxValue = 0;
        for(auto dataValue:yearDataArr){
            int data = dataValue.toInt();
            if(data > maxValue){
                maxValue =data;
            }
        }
        dataObj.insert("maxValueOfYear",maxValue);
//        qDebug()<<"getSellData:"<<"dataObj>>"<<dataObj;
        emit sigGetSellData(true,dataObj);
    });
}

void DataHandleVM::getProjectData()
{
    QtConcurrent::run([=](){
        QFile localDataFile("./chartLocalData.json");
        if(!localDataFile.open(QIODevice::ReadOnly)){
            qDebug()<<"打开本地数据文件失败";
            emit sigGetProjectData(false,QJsonObject());
        }
        QJsonDocument datDoc = QJsonDocument::fromJson(localDataFile.readAll());
        QJsonObject dataObj = datDoc.object().value("projectData").toObject();
//        qDebug()<<"getProjectData:"<<"dataObj>>"<<dataObj;
        emit sigGetProjectData(true,dataObj);
    });
}

void DataHandleVM::getPerformanceData()
{
    QtConcurrent::run([=](){
        QThread::currentThread();
        QFile localDataFile("./chartLocalData.json");
        if(!localDataFile.open(QIODevice::ReadOnly)){
            qDebug()<<"打开本地数据文件失败";
            emit sigGetPerformanceData(false,QJsonObject());
        }
        QJsonDocument datDoc = QJsonDocument::fromJson(localDataFile.readAll());
        QJsonObject dataObj = datDoc.object().value("performanceData").toObject();
//        qDebug()<<"getPerformanceData:"<<"dataObj>>"<<dataObj;
        emit sigGetPerformanceData(true,dataObj);
    });
}

void DataHandleVM::loadPeopelModelData(ECusTableModel *model)
{
    QFile localDataFile("./tableLocalData.json");
    if(!localDataFile.open(QIODevice::ReadOnly)){
        qDebug()<<"打开本地数据文件失败";
        emit sigLoadPeopelModelData(false);
    }
    QJsonDocument datDoc = QJsonDocument::fromJson(localDataFile.readAll());
    localDataFile.close();
    QJsonObject dataObj = datDoc.object().value("personManage").toObject();
    QJsonArray dataArr = dataObj.value("tableData").toArray();
    QList<QVariantList> dataList;
    for(auto rowArrValue: dataArr) {
        QVariantList rowDataList = rowArrValue.toArray().toVariantList();
        dataList.push_back(rowDataList);
    }
    //        qDebug()<<"loadPeopelModelData dataList: "<<dataList;
    model->loadData(dataList);
    emit sigLoadPeopelModelData(true);
}

bool DataHandleVM::insertNewRow(ECusTableModel *model, QVariantList dataList)
{
    if(model != nullptr && dataList.length()>0){
        model->insertNewRowData(dataList);
        return  true;
    }
    else
        return  false;
}

bool DataHandleVM::removeRow(ECusTableModel *model, int row)
{
    if(row >=0){
        model->removeRowData(row);
        return true;
    }
    else
        return false;
}

QVariant DataHandleVM::getRowData(ECusTableModel *model, int row, QString role)
{
    if(row >=0){
        return  model->rowData(row,role);
    }
    else
        return QVariant();
}

void DataHandleVM::setRowData(ECusTableModel *model, int row, QJsonObject dataObj)
{
    for(int i =0;i<dataObj.count();++i){
        QString role = dataObj.keys().value(i);
        QVariant newValue = dataObj.value(role);
        model->setRowData(row,role,newValue);
    }
}



