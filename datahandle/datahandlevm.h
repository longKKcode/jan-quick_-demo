#ifndef DATAHANDLEVM_H
#define DATAHANDLEVM_H

#include <QObject>
#include <QJsonObject>
#include <QJsonArray>

#include <EControl/model/ECusTableModel.h>

#include "../sqlite/sqlitehandle.h"
class DataHandleVM : public QObject
{
    Q_OBJECT
public:
    explicit DataHandleVM(QObject *parent = nullptr);
    ~DataHandleVM();

    //注册用户函数
    Q_INVOKABLE bool registerAccount(QJsonObject userInfo);
    //登录账户密码校验
    Q_INVOKABLE bool checkAccount(QString account,QString password);
    //根据登录账户获取账户其它信息(登录后使用)
    Q_INVOKABLE QVariant getInfoByUser(QString account,QString infoKey);

    //获取资产配置饼图的数据
    Q_INVOKABLE void getAssetPieChartData();
    //获取年销售数据
    Q_INVOKABLE void getSellData();
    //获取项目数据
    Q_INVOKABLE void getProjectData();
    //获取年度表现数据
    Q_INVOKABLE void getPerformanceData();

    //加载人员管理表格model数据
    Q_INVOKABLE void loadPeopelModelData(ECusTableModel* model);
    //人员表格model增加行
    Q_INVOKABLE bool insertNewRow(ECusTableModel* model,QVariantList dataList);
    //人员表格model删除行
    Q_INVOKABLE bool removeRow(ECusTableModel* model,int row);
    //根据行和role获取数据
    Q_INVOKABLE QVariant getRowData(ECusTableModel* model,int row,QString role);
    //修改选中的人员信息
    Q_INVOKABLE void setRowData(ECusTableModel* model,int row,QJsonObject dataObj);
public:
    SqliteHandle *m_sqlHandle;
signals:
    void sigGetAssetPieData(bool isSuccess,QJsonObject dataObj);
    void sigGetSellData(bool isSuccess,QJsonObject dataObj);
    void sigGetProjectData(bool isSuccess,QJsonObject dataObj);
    void sigGetPerformanceData(bool isSuccess,QJsonObject dataObj);
    void sigLoadPeopelModelData(bool isSuccess);
public slots:

private:
};

#endif // DATAHANDLEVM_H
